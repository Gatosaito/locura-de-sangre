using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Flame : MonoBehaviour
{
    float moveSpeed;
    Rigidbody2D rg2d;
    Vector2 moveDirection;

    PlayerMovenment target;

    private void Start()
    {
        moveSpeed = GetComponent<Boss>().speed;
        rg2d = GetComponent<Rigidbody2D>();
        target = PlayerMovenment.instance;

        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rg2d.velocity = new Vector2(moveDirection.x, moveDirection.y);

    }
}
