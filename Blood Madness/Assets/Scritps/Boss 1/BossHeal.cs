using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHeal : MonoBehaviour
{
    Boss bossH;
    public bool isDamage;
    public GameObject deathEffect;


    public void Start()
    {
        bossH = GetComponent<Boss>();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Weapon") && isDamage)
        {
            bossH.healthPoints -= 1f;
            StartCoroutine(Damager());

            if(bossH.healthPoints <= 0)
            {
                Instantiate(deathEffect, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }

    IEnumerator Damager()
    {
        isDamage = true;
        yield return new WaitForSeconds(0.5f);
        isDamage = false;
    }
}
