using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBehavior : MonoBehaviour
{

    public Transform[] transforms;

    public GameObject flame;

    public float timeToShoot, countdown;
    public float timeToTP, countdownToTP;

    public float bossHealth, currentHealth; 
    public Image HealthIgm;
    // Start is called before the first frame update

    private void Start()
    {
        var InicialPosition = Random.Range(0, transforms.Length);
        transform.position = transforms[InicialPosition].position;
        countdown = timeToShoot;
        countdownToTP = timeToTP;
    } 

    private void Update()
    {
        Countdown();
        Damage();
        BossScale();
    }   

    public void Countdown()
    {
        countdown -= Time.deltaTime;
        countdownToTP -= Time.deltaTime;
        if (countdown < 0)
        {
            ShootPlayer();
            countdown = timeToShoot;
            Teleport();
        }

        if (countdownToTP <= 0)
        {
            countdownToTP = timeToTP;
            Teleport();
        }

    }

    public void ShootPlayer()
    {
        GameObject spell = Instantiate(flame, transform.position, Quaternion.identity);
    }

    public void Teleport()
    {
        var InicialPosition = Random.Range(0, transforms.Length);
        transform.position = transforms[InicialPosition].position;
    }

    public void Damage()
    {
        currentHealth = GetComponent<Boss>().healthPoints;
        HealthIgm.fillAmount = currentHealth / bossHealth;
    }

    private void OnDestroy()
    {
        BossUI.instance.BossDesactivator();
    }

    public  void BossScale()
    {
        if(transform.position.x > PlayerMovenment.instance.transform.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
