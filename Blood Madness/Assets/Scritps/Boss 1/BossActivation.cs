using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossActivation : MonoBehaviour
{

    public GameObject boss;

    private void Start()
    {
        boss.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            BossUI.instance.BossActivation();

            StartCoroutine(WaitForBoss());
        }
    }

    IEnumerator WaitForBoss()
    {
        var currentSpeed  =  PlayerMovenment.instance.velocidad;
        PlayerMovenment.instance.velocidad = 0;
        boss.SetActive(true); 
        yield return new WaitForSeconds(3f);
        PlayerMovenment.instance.velocidad = currentSpeed;
        Destroy(gameObject);
    }
}
