using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IA_Emeny : MonoBehaviour
{
    public Animator anim;

    [SerializeField]
    Transform player;

    //distancia player
    [SerializeField]
    float rangoAgro;
    public float velocidadMov;
    
    private bool miraIzquierda;

    Rigidbody2D rb2d;

    public SpriteRenderer sprite;

    private void Start()
    {
        miraIzquierda = true;
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        // distancia player

        float distanJugador = Vector2.Distance(transform.position, player.position);
       // Debug.Log("Distancia del jugador: " + distanJugador);

        if (distanJugador < rangoAgro && Mathf.Abs(distanJugador) > 1)
        {
            //perseguir
            PerseguirJugador();
            anim.SetFloat("Velocidad", 1);
            anim.SetBool("Ataca", false);
        }
        else if(Mathf.Abs(distanJugador) < 1)
        {
            anim.SetBool("Ataca",true);
        }
        else
        {
            NoPerseguir();
            anim.SetFloat("Velocidad", 0);
        }

    }

    private void NoPerseguir()
    {
        rb2d.velocity = Vector2.zero;
    }

    private void PerseguirJugador()
    {
        /*
        //si estamos a la izquierda del jugador entonces movemos el enemigo hacia la derecha
        if (transform.position.x < player.position.x && !miraIzquierda)
        {
            rb2d.velocity = new Vector2(velocidadMov, 0f);
            Flip();
        }
        else if (transform.position.x > player.position.x && !miraIzquierda) 
        {
            rb2d.velocity = new Vector2(-velocidadMov,0f);
            Flip();
        }
        else if(!miraIzquierda)
        {
            rb2d.velocity = new Vector2(velocidadMov, 0f);
        }
        else if (miraIzquierda)
        {
            rb2d.velocity = new Vector2(velocidadMov, 0f);
        }
        */

        if(transform.position.x > player.position.x)
        {
            sprite.flipX = true;
            rb2d.velocity = new Vector2(-velocidadMov, 0f);
        }
        if(transform.position.x < player.position.x)
        { 
            sprite.flipX = false;
            rb2d.velocity = new Vector2(velocidadMov, 0f);
        }
    }

    private void Flip()
    {
        //defino donde mira nuevamente estando el jugador
        miraIzquierda = !miraIzquierda;

        //multiplicar la escala en x del personaje por -1
        Vector3 laEscala = transform.localScale;
        laEscala.x *= -1;
        transform.localScale = laEscala;
    }
}
