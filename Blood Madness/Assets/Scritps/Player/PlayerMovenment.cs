using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovenment : MonoBehaviour
{
    public float velocidad;
    public float fuerzaSalto;
    public LayerMask capaSuelo;
     
    private new Rigidbody2D rigidbody;
    private BoxCollider2D boxCollider;
    private bool mirandoDrc = true;
    private Animator animator;

    public static PlayerMovenment instance;


    public void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Jump();
        Attack();
    }

    void Movement() 
    {
        float inputMovimiento = Input.GetAxis("Horizontal");

        if (inputMovimiento != 0)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        rigidbody.velocity = new Vector2(inputMovimiento * velocidad, rigidbody.velocity.y);
        Orientacion(inputMovimiento);
    }

    bool EstaEnSuelo() 
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, new Vector2(boxCollider.bounds.size.x, boxCollider.bounds.size.y), 0f, Vector2.down, 0.2f, capaSuelo);
        
        return raycastHit.collider != null;
    }
    void Attack()
    {
        if(Input.GetButtonDown("Fire1"))
        {

            animator.SetBool("Attack", true);
        }
        else
        {
            animator.SetBool("Attack", false);
        }

    }
    void Jump() 
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnSuelo())
        {
            animator.SetBool("isJumping", true);

            rigidbody.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }
        else
        {
            animator.SetBool("isJumping", false);

        }
    }

    public void Orientacion(float inputMovimiento) 
    {
        if ((mirandoDrc == true && inputMovimiento < 0) || (mirandoDrc == false && inputMovimiento > 0))
        {
            mirandoDrc = !mirandoDrc;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }       
    }
}
